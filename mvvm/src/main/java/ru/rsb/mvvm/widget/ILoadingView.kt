package ru.rsb.mvvm.widget

import android.graphics.drawable.Drawable
import android.widget.ProgressBar
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView

/**
 * @author Vasilyev Evgeny.
 */
interface ILoadingView {
    /**
     * Скрыть прогресс бар и сообщение и показать контент
     * */
    fun clearScreen()

    fun clearLoading()
    fun clearMessage()
    /**
     * Показать окно загрузки
     * @param value - строковая константа из strings.xml
     * */
    fun showLoading(@StringRes value: Int)

    /**
     * Показать окно загрузки с сообщением
     * @param value - строка с сообщением
     * */
    fun showLoading(value: String)

    /**Показать окно загрузки без сообщения*/
    fun showLoading()

    /**
     * Спрятать прогресс бар и показывать сообщение.
     * @param message - строка с сообщением
     * @param image - картинка
     * */
    fun showMessage(message: String, image: Drawable?)

    /**
     *  Спрятать прогресс бар и показать сообщение с картинкой.
     *  @param message - строка с сообщением
     *  @param image - константа из res/drawable
     * */
    fun showMessage(message: String, @DrawableRes image: Int)

    /**
     * Спрятать прогресс бар и показать сообщение с картинкой.
     * @param message - строковая константа из strings.xml
     * @param image - картинка
     * */
    fun showMessage(@StringRes message: Int, image: Drawable?)

    /**
     * Спрятать прогресс бар и показать сообщение с картинкой.
     * @param message - строковая константа из strings.xml
     * @param image - константа из res/drawable
     * */
    fun showMessage(@StringRes message: Int, @DrawableRes image: Int)

    /**
     * Спрятать прогресс бар и показать сообщение.
     * @param message - строковая константа из strings.xml
     */
    fun showMessage(@StringRes message: Int)

    /**
     * Спрятать прогресс бар и показывать сообщение.
     * @param message - строка с сообщением
     */
    fun showMessage(message: String)
}