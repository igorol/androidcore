package ru.rsb.mvvm.widget

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import ru.rsb.mvvm.R

/**
 * @author Maxim Berezin
 */
class LoadingView : FrameLayout, ILoadingView {
    /** [AppCompatTextView] содержащий сообщением об ошибке*/
    var messageTextView: AppCompatTextView? = null
        private set(value) {
            field = value
        }

    /**[AppCompatImageView] содержащий картинку во время показа сообщения*/
    var messageImageView: AppCompatImageView? = null
        private set(value) {
            field = value
        }

    /**[ProgressBar] который появляется во время загрузки*/
    var loadingProgressBar: ProgressBar? = null
        private set(value) {
            field = value
        }

    /**[AppCompatTextView] содержащий сообщение во время загрузки*/
    private var loadingTextView: AppCompatTextView? = null

    private var loadingAnimator: AnimatorSet? = null
    private var messageAnimator: AnimatorSet? = null

    companion object {
        const val ANIMATION_DURATION = 300L
    }

    constructor(context: Context) : super(context) {
        initLoading(context)
        obtainAppThemeColors(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initLoading(context)
        obtainAppThemeColors(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initLoading(context)
        obtainAppThemeColors(context)
    }

    private fun initLoading(context: Context) {
        val inflater = LayoutInflater.from(context)

        //разметка с сообщением и загрузкой
        val loadingViewGroup = inflater.inflate(R.layout.loading_screen, this, false) as ConstraintLayout
        loadingViewGroup.visibility = View.GONE
        loadingViewGroup.id = R.id.loadingView

        loadingTextView = loadingViewGroup.getChildAt(0) as AppCompatTextView
        loadingProgressBar = loadingViewGroup.getChildAt(1) as ProgressBar

        //разметка с сообщением и картинкой
        val viewGroup = inflater.inflate(R.layout.error_screen, this, false) as ConstraintLayout
        viewGroup.visibility = View.GONE

        messageTextView = viewGroup.getChildAt(0) as AppCompatTextView
        messageImageView = viewGroup.getChildAt(1) as AppCompatImageView

        this.addView(loadingViewGroup, 0)
        this.addView(viewGroup, 1)
    }

    private fun obtainAppThemeColors(context: Context) {

        val colorAccent = TypedValue().apply {
            context.theme.resolveAttribute(R.attr.colorAccent, this, true)
        }.data

        //применяем атрибуты
        setAccentColor(colorAccent)
    }

    /**
     * Показываем или прячем прогресс бар.
     * @param visible true  - показать прогресс бар и скрыть контент
     *                false - скрыть прогресс бар и показать контент
     */
    private fun setLoadingVisible(visible: Boolean) {
        val loadingView = getChildAt(0)
        val contentView = getChildAt(2)
        loadingAnimator?.run { cancelAnimation(this) }
        loadingAnimator = AnimatorSet().apply {
            playTogether(
                if (visible) mutableListOf<Animator>(
                    createRevealAnimation(loadingView),
                    createHideAnimation(contentView)
                ) else mutableListOf<Animator>(
                    createRevealAnimation(contentView),
                    createHideAnimation(loadingView)
                )
            )
            start()
        }
    }

    /**
     * Показываем или прячем сообщение.
     * @param visible true  - показать сообщение и скрыть контент
     *                false - скрыть сообщение и показать контент
     */
    private fun setMessageVisible(visible: Boolean) {
        val messageView = getChildAt(1)
        val contentView = getChildAt(2)
        messageAnimator?.run { cancelAnimation(this) }
        messageAnimator = AnimatorSet().apply {
            playTogether(
                if (visible) mutableListOf<Animator>(
                    createRevealAnimation(messageView),
                    createHideAnimation(contentView)
                )
                else mutableListOf<Animator>(createRevealAnimation(contentView), createHideAnimation(messageView))
            )
            start()
        }
    }

    /**
     * Скрыть прогресс бар и сообщение и показать контент
     * */
    override fun clearScreen() {
        val loadingView = getChildAt(0)
        val messageView = getChildAt(1)

        if (loadingView.visibility == View.VISIBLE) {
            clearLoading()
        }

        if (messageView.visibility == View.VISIBLE) {
            clearMessage()
        }
    }

    override fun clearLoading() {
        setLoadingVisible(false)
    }

    override fun clearMessage() {
        setMessageVisible(false)
    }

    /**
     * Установить цвет выделения. Влияет на цвет ProgressBar, цвет текста загрузки и цвет текста ошибки.
     * @param color - новый цвет
     * */
    private fun setAccentColor(@ColorInt color: Int) {
        loadingTextView?.setTextColor(color)
        messageTextView?.setTextColor(color)
        loadingProgressBar?.indeterminateDrawable?.setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY)
    }

    /**
     * Показать окно загрузки
     * @param value - строковая константа из strings.xml
     * */
    override fun showLoading(@StringRes value: Int) {
        showLoading(context.getString(value))
    }

    /**
     * Показать окно загрузки с сообщением
     * @param value - строка с сообщением
     * */
    override fun showLoading(value: String) {
        showLoading()
        loadingTextView?.apply {
            visibility = View.VISIBLE
            text = value
        }
    }

    /**Показать окно загрузки без сообщения*/
    override fun showLoading() {
        loadingTextView?.apply {
            text = null
            visibility = View.GONE
        }
        setLoadingVisible(true)
    }

    /**
     * Спрятать прогресс бар и показывать сообщение.
     * @param message - строка с сообщением
     * @param image - картинка
     * */
    override fun showMessage(message: String, image: Drawable?) {
        showMessage(message)
        messageImageView?.setImageDrawable(image)
    }

    /**
     *  Спрятать прогресс бар и показать сообщение с картинкой.
     *  @param message - строка с сообщением
     *  @param image - константа из res/drawable
     * */
    override fun showMessage(message: String, @DrawableRes image: Int) {
        showMessage(message)
        messageImageView?.setImageResource(image)
    }

    /**
     * Спрятать прогресс бар и показать сообщение с картинкой.
     * @param message - строковая константа из strings.xml
     * @param image - картинка
     * */
    override fun showMessage(@StringRes message: Int, image: Drawable?) {
        showMessage(message)
        messageImageView?.setImageDrawable(image)
    }

    /**
     * Спрятать прогресс бар и показать сообщение с картинкой.
     * @param message - строковая константа из strings.xml
     * @param image - константа из res/drawable
     * */
    override fun showMessage(@StringRes message: Int, @DrawableRes image: Int) {
        showMessage(message)
        messageImageView?.setImageResource(image)
    }

    /**
     * Спрятать прогресс бар и показать сообщение.
     * @param message - строковая константа из strings.xml
     */
    override fun showMessage(@StringRes message: Int) {
        showMessage(context.getString(message))
    }

    /**
     * Спрятать прогресс бар и показывать сообщение.
     * @param message - строка с сообщением
     */
    override fun showMessage(message: String) {
        //убрать картинку
        messageImageView?.setImageDrawable(null)
        messageTextView?.text = message
        setMessageVisible(true)
    }

    private fun createHideAnimation(view: View): ObjectAnimator {
        view.animate()
        view.clearAnimation()
        return ObjectAnimator.ofFloat(view, "alpha", 0f).apply {
            duration = ANIMATION_DURATION
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    view.visibility = View.GONE
                    this@apply.removeAllListeners()
                }
            })
        }
    }

    private fun createRevealAnimation(view: View): ObjectAnimator {
        view.clearAnimation()
        return ObjectAnimator.ofFloat(view, "alpha", 0f, 1f).apply {
            view.visibility = View.VISIBLE
            duration = ANIMATION_DURATION
            removeAllListeners()
        }
    }

    private fun cancelAnimation(animatorSet: AnimatorSet) {
        animatorSet.removeAllListeners()
        animatorSet.end()
        animatorSet.cancel()
    }
    //endregion
}