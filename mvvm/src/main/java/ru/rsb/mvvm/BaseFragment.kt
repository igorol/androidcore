package ru.rsb.mvvm

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import ru.rsb.mvvm.extensions.showSnackBar
import ru.rsb.mvvm.helpers.EventObserver
import ru.rsb.mvvm.interfaces.BaseMVVMLifecycle
import ru.rsb.mvvm.interfaces.BaseMVVMLoading
import ru.rsb.mvvm.interfaces.FragmentMVVMLifecycle
import ru.rsb.mvvm.interfaces.ViewSnackbar
import ru.rsb.mvvm.viewmodels.BaseViewModel

/**
 * @author Vasilyev Evgeny.
 */
abstract class BaseFragment<T : BaseViewModel> : Fragment(),
    BaseMVVMLoading,
    ViewSnackbar,
    BaseMVVMLifecycle<T> {

    protected lateinit var viewModel: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = onCreateViewModel()
        onObserveBaseFields(viewModel)
        onObserve(viewModel)
    }

    //region BaseMVVMLifecycle
    override fun onObserveBaseFields(viewModel: T) {
        viewModel.apply {
            showSnack.observe(this@BaseFragment, EventObserver { message -> onSnack(message) })
            showLoading.observe(this@BaseFragment, Observer { showLoading -> onLoading(showLoading) })
            state.observe(this@BaseFragment, Observer {
                when (it.newState) {
                    BaseViewModel.ViewState.CONTENT -> onContent()
                    BaseViewModel.ViewState.ERROR -> onError(it.error)
                }
            })
        }
    }

    override fun onObserve(viewModel: T) {}

    override fun onSnack(message: String) = showSnack(message)

    override fun onLoading(showLoading: Boolean) = showLoading(showLoading)

    override fun onContent() = showContent()

    override fun onError(event: BaseViewModel.ErrorMessageEvent?) {
        when {
            event == null -> showError(R.string.base_loading_state_activity_error_occur)
            !(event.errorMessage == 0 && event.errorImage == 0) -> showError(event.errorMessage, event.errorImage)
            event.errorMessage != 0 -> showError(event.errorMessage)
            else -> showError(R.string.base_loading_state_activity_error_occur)
        }
    }
    //endregion

    override fun showSnack(message: Int) {
        showSnack(getString(message))
    }

    override fun showSnack(message: String) {
        view?.showSnackBar(message, resources.getInteger(R.integer.snack_delay))
    }
}