package ru.rsb.mvvm.helpers

import androidx.lifecycle.Observer

/**
 * @author Vasilyev Evgeny.
 */

class EventObserver<T>(private val onEventUnhandledContent: (T) -> Unit) : Observer<Event<T>> {
    override fun onChanged(event: Event<T>?) {
        event?.getContentIfNotHandled()?.let { value ->
            onEventUnhandledContent(value)
        }
    }
}