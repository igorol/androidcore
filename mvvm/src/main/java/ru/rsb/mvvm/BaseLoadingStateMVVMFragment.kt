package ru.rsb.mvvm

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import ru.rsb.mvvm.interfaces.FragmentMVVMLifecycle
import ru.rsb.mvvm.viewmodels.BaseViewModel
import ru.rsb.mvvm.widget.LoadingView

/**
 * @author Vasilyev Evgeny.
 */
abstract class BaseLoadingStateMVVMFragment<T : BaseViewModel> :
    BaseFragment<T>(), FragmentMVVMLifecycle {
    private lateinit var loadingView: LoadingView

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loadingView = LoadingView(context!!).apply {
            layoutParams =
                FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        }
        val contentView = onPreCreateView(inflater, loadingView, savedInstanceState)

        return loadingView.apply { addView(contentView) }
    }

    override fun showLoading(loading: Boolean) {
        if (loading) loadingView.showLoading()
        else loadingView.clearLoading()
    }

    override fun showContent() = loadingView.clearScreen()

    override fun showError(message: String, image: Drawable?) = loadingView.showMessage(message, image)

    override fun showError(message: String, image: Int) = loadingView.showMessage(message, image)

    override fun showError(message: Int, image: Drawable?) = loadingView.showMessage(message, image)

    override fun showError(message: Int, image: Int) = loadingView.showMessage(message, image)

    override fun showError(message: Int) = loadingView.showMessage(message)

    override fun showError(message: String) = loadingView.showMessage(message)
}