package ru.rsb.mvvm.viewmodels

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ru.rsb.mvvm.helpers.Event

/**
 * @author Vasilyev Evgeny.
 */
abstract class BaseViewModel : ViewModel() {

    abstract val state: MediatorLiveData<ViewState>

    val showSnack: MutableLiveData<Event<String>> by lazy {
        MutableLiveData<Event<String>>()
    }

    val showLoading: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    /**
     * Текущее состояние View
     * Атрибут, отвечающий за загрузку, намеренно исключён потому что он опционален для каждого состояния
     * @param newState - новое состояние, которое должно быть отображено на экране. Значение из [STATES]
     * */
    data class ViewState(val newState: Int, val error: ErrorMessageEvent?) {
        companion object STATES {
            const val CONTENT = 0
            const val ERROR = 1
        }
    }

    data class ErrorMessageEvent(val errorMessage: Int, val errorImage: Int = 0)
}