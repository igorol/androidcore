package ru.rsb.mvvm.viewmodels

import android.annotation.SuppressLint
import android.app.Application

/**
 * @author Vasilyev Evgeny.
 */
abstract class BaseAndroidViewModel<T: Application>(@SuppressLint("StaticFieldLeak") var application: T) : BaseViewModel()