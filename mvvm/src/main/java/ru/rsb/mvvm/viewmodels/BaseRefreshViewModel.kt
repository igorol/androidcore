package ru.rsb.mvvm.viewmodels

/**
 * @author Vasilyev Evgeny.
 */
abstract class BaseRefreshViewModel : BaseViewModel() {
    /**Обновить контент*/
    abstract fun onRefresh()
}