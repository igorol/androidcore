package ru.rsb.mvvm

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ListView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.swipe_refresh_screen.*
import kotlinx.android.synthetic.main.swipe_refresh_screen.view.*
import ru.rsb.mvvm.interfaces.BaseSwipeRefreshMVVMLifecycle
import ru.rsb.mvvm.interfaces.FragmentMVVMLifecycle
import ru.rsb.mvvm.viewmodels.BaseRefreshViewModel

/**
 * @author Vasilyev Evgeny.
 */
abstract class BaseSwipeToRefreshMVVMFragment<T : BaseRefreshViewModel> :
    BaseFragment<T>(),
    FragmentMVVMLifecycle,
    BaseSwipeRefreshMVVMLifecycle {

    private var contentView: View? = null

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val containerView = inflater.inflate(R.layout.swipe_refresh_screen, container, false) as FrameLayout

        val view = onPreCreateView(inflater, swipeRefreshLayout, savedInstanceState)

        if (!(view is RecyclerView || view is ListView)) {
            throw IllegalArgumentException("onPreCreateView должен вернуть View одного из следующих типов: ListView, RecyclerView")
        }

        containerView.swipeRefreshLayout.apply {
            addView(view)
            setOnRefreshListener { viewModel.onRefresh() }
        }

        onCreateContentView(inflater, container, savedInstanceState)?.let {
            contentView = it
            containerView.addView(it)
        }

        return containerView
    }
    //endregion

    fun getSwipeRefreshLayout(): SwipeRefreshLayout {
        return swipeRefreshLayout
    }

    override fun onCreateContentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return null
    }

    //region BaseMVVMLoading
    override fun showLoading(loading: Boolean) {
        if (swipeRefreshLayout.isRefreshing != loading)
            swipeRefreshLayout.isRefreshing = loading
    }

    override fun showContent() {
        hideErrorView()
        showContentView()
    }

    override fun showError(message: Int, image: Drawable?) = showError(getString(message), image)

    override fun showError(message: String, image: Drawable?) {
        messageText.text = message
        messageDrawable.background = image
        messageDrawable.visibility = View.VISIBLE
        showErrorView()
        hideContentView()
    }

    override fun showError(message: Int, image: Int) = showError(getString(message), image)

    override fun showError(message: String, image: Int) {
        messageText.text = message
        messageDrawable.setImageResource(image)
        messageDrawable.visibility = View.VISIBLE
        showErrorView()
        hideContentView()
    }

    override fun showError(message: Int) = showError(getString(message))

    override fun showError(message: String) {
        messageText.text = message
        messageDrawable.setImageDrawable(null)
        messageDrawable.visibility = View.GONE
        showErrorView()
        hideContentView()
    }
    //endregion

    private fun hideErrorView() {
        errorView.visibility = View.GONE
    }

    private fun showErrorView() {
        errorView.visibility = View.VISIBLE
    }

    private fun hideContentView() {
        for (i in 0 until swipeRefreshLayout.childCount){
            swipeRefreshLayout.getChildAt(i).visibility = View.GONE
        }
        contentView?.visibility = View.GONE
    }

    private fun showContentView() {
        for (i in 0 until swipeRefreshLayout.childCount){
            swipeRefreshLayout.getChildAt(i).visibility = View.VISIBLE
        }
        contentView?.visibility = View.VISIBLE
    }
}