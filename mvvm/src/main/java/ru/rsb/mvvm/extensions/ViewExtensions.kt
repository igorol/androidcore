package ru.rsb.mvvm.extensions

import android.view.View
import androidx.annotation.StringRes
import com.google.android.material.snackbar.Snackbar

/**
 * @author Vasilyev Evgeny.
 */

/**
 *Показываем SnackBar
 */
fun View.showSnackBar(message: String, duration: Int) {
    Snackbar.make(this, message, duration).show()
}

fun View.showSnackBar(@StringRes message: Int, duration: Int) {
    Snackbar.make(this, message, duration)
}