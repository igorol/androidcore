package ru.rsb.mvvm.interfaces

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import ru.rsb.mvvm.viewmodels.BaseViewModel
import ru.rsb.mvvm.viewmodels.BaseViewModel.*

/**
 * @author Vasilyev Evgeny.
 */
interface BaseMVVMLifecycle<T : BaseViewModel> {
    /**
     * Вызывается в методе [Fragment.onCreate] или в [AppCompatActivity.onCreate] всегда до [onObserveBaseFields]
     * Должен вернуть объект типа [BaseViewModel]
     * */
    fun onCreateViewModel(): T

    /**
     * Подписка на стандартные поля BaseViewModel
     * Вызывается в методе [Fragment.onCreate]
     * */
    fun onObserveBaseFields(viewModel: T)

    /**
     * Вызывается, когда View принимает состояние [BaseViewModel.ViewState.ERROR]
     * */
    fun onError(event: ErrorMessageEvent?)

    /**
     * Вызывается, когда View принимает состояние [BaseViewModel.ViewState.CONTENT]
     * Не передаётся дата, т.к. заранее неизвестен её тип и преднозначение.
     * Получать данные следует из ViewModel, подписавшись в методе [onObserve]
     * */
    fun onContent()

    /**
     * Вызывается во время получения события из поля [BaseViewModel.showLoading]
     * */
    fun onLoading(showLoading: Boolean)

    /**
     * Вызывается во время получения события из поля [BaseViewModel.showSnack]
     * */
    fun onSnack(message: String)

    /**
     * Вызывается в методе [Fragment.onCreate] или в [AppCompatActivity.onCreate] всегда после [onObserveBaseFields]
     * @param viewModel - модель, которая была получена в методе [onCreateViewModel]
     * */
    fun onObserve(viewModel: T)
}