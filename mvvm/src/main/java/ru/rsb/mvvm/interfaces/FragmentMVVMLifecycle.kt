package ru.rsb.mvvm.interfaces

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

/**
 * @author Vasilyev Evgeny.
 */
interface FragmentMVVMLifecycle {
    /**
     * Вызывается внутри метода [Fragment.onCreateView].
     * Из-за особенностей фреймворка инициализация разметки следует проводить в этом методе
     *
     * Параметры однозначно передаются из метода [Fragment.onCreateView] кроме
     * @param container - обертка, в которой будет находиться view
     *
     * @return view с разметкой
     * */
    fun onPreCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View

}