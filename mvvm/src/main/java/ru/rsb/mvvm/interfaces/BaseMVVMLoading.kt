package ru.rsb.mvvm.interfaces

import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

/**
 * @author Vasilyev Evgeny.
 */
interface BaseMVVMLoading {
    /**
     * Показать контент и скрыть ошибку
     * */
    fun showContent()

    /**
     * Показать/скрыть загрузку
     * @param loading если true, то показать загрузку. false - скрыть загрузку
     * */
    fun showLoading(loading: Boolean)

    /**
     * Показавать ошибку и скрыть контент
     * @param message - строка с сообщением
     * @param image - картинка
     * */
    fun showError(message: String, image: Drawable?)

    /**
     * Показавать ошибку и скрыть контент
     *  @param message - строка с сообщением
     *  @param image - константа из res/drawable
     * */
    fun showError(message: String, @DrawableRes image: Int)

    /**
     * Показавать ошибку и скрыть контент
     * @param message - строковая константа из strings.xml
     * @param image - картинка
     * */
    fun showError(@StringRes message: Int, image: Drawable?)

    /**
     * Показавать ошибку и скрыть контент
     * @param message - строковая константа из strings.xml
     * @param image - константа из res/drawable
     * */
    fun showError(@StringRes message: Int, @DrawableRes image: Int)

    /**
     * Показавать ошибку и скрыть контент
     * @param message - строковая константа из strings.xml
     */
    fun showError(@StringRes message: Int)

    /**
     * Показавать ошибку и скрыть контент
     * @param message - строка с сообщением
     */
    fun showError(message: String)
}