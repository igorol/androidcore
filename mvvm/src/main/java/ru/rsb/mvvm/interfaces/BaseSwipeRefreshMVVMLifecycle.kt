package ru.rsb.mvvm.interfaces

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

/**
 * @author Vasilyev Evgeny.
 */
interface BaseSwipeRefreshMVVMLifecycle {
    /**
     *  Вызывается для создания View с контентом. Вызывается всегда после [FragmentMVVMLifecycle.onPreCreateView] внутри метода [Fragment.onCreateView]
     *  Все параметры передаются соответственно из метода [Fragment.onCreateView]
     *  View, полученная из этого метода, всегда будет поверх списка SwipeRefreshLayout
     * */
    fun onCreateContentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
}