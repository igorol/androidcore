package ru.rsb.mvvm.interfaces

import androidx.annotation.StringRes

/**
 * @author Vasilyev Evgeny.
 */
interface ViewSnackbar {
    fun showSnack(@StringRes message: Int)

    fun showSnack(message: String)
}