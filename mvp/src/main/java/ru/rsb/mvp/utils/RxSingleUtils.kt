package ru.rsb.mvp.utils

import io.reactivex.Single
import io.reactivex.SingleTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.annotations.CheckReturnValue
import io.reactivex.annotations.SchedulerSupport
import io.reactivex.schedulers.Schedulers
import ru.rsb.mvp.LoadingStateMvpView
import ru.rsb.mvp.RxPresenter

/**
 * Утильный класс для работы с синглами
 *
 * @author Maxim Berezin
 */

@CheckReturnValue
@SchedulerSupport(SchedulerSupport.NONE)
fun <T : Any> Single<T>.applyUiDefaults(view: LoadingStateMvpView?, presenter: RxPresenter): Single<T> {
    return this.compose(RxSingleUtils.applySchedulers<T>())
            .compose(RxSingleUtils.showLoadingView<T>(view))
            .compose(RxSingleUtils.addToCompositeDisposable<T>(presenter))
}

object RxSingleUtils {

    fun <T> applyUIDefaults(view: LoadingStateMvpView?, presenter: RxPresenter): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream
                    .compose(RxSingleUtils.applySchedulers<T>())
                    .compose(RxSingleUtils.showLoadingView<T>(view))
                    .compose(RxSingleUtils.addToCompositeDisposable<T>(presenter))
        }
    }

    fun <T> showLoadingView(view: LoadingStateMvpView?): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream
                    .doOnSubscribe { view?.setLoadingViewVisible(true) }
                    .doOnSuccess { view?.setLoadingViewVisible(false) }
        }
    }

    fun <T> applyUIDefaults(presenter: RxPresenter): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream
                    .compose(RxSingleUtils.applySchedulers<T>())
                    .compose(RxSingleUtils.addToCompositeDisposable<T>(presenter))
        }
    }

    fun <T> applySchedulers(): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }

    fun <T> addToCompositeDisposable(presenter: RxPresenter): SingleTransformer<T, T> {
        return SingleTransformer { upstream ->
            upstream
                    .doOnSubscribe { presenter.getCompositeDisposable().add(it) }
        }
    }
}