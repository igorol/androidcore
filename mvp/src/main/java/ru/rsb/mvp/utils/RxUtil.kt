package ru.rsb.mvp.utils

import io.reactivex.ObservableTransformer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.rsb.mvp.LoadingStateMvpView
import ru.rsb.mvp.RxPresenter

/**
 * @author Maxim Berezin
 */
object RxUtil {

    fun <T> applyUIDefaults(view: LoadingStateMvpView?, presenter: RxPresenter): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream
                    .compose(RxUtil.applySchedulers<T>())
                    .compose(RxUtil.showLoadingView<T>(view))
                    .compose(RxUtil.addToCompositeDisposable<T>(presenter))
        }
    }

    private fun <T> showLoadingView(view: LoadingStateMvpView?): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream
                    .doOnSubscribe { view?.setLoadingViewVisible(true) }
                    .doOnNext { view?.setLoadingViewVisible(false) }
        }
    }

    fun <T> applyUIDefaults(presenter: RxPresenter): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream
                    .compose(RxUtil.applySchedulers<T>())
                    .compose(RxUtil.addToCompositeDisposable<T>(presenter))
        }
    }

    fun <T> applySchedulers(): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
        }
    }

    private fun <T> addToCompositeDisposable(presenter: RxPresenter): ObservableTransformer<T, T> {
        return ObservableTransformer { upstream ->
            upstream
                    .doOnSubscribe { presenter.getCompositeDisposable().add(it) }
        }
    }
}
