package ru.rsb.mvp

/**
 * Базовый интерфейс для реализации процесса загрузки и отображения ошибок.
 * @author Maxim Berezin
 */
interface LoadingStateMvpView : MvpView {

    fun setLoadingViewVisible(visible: Boolean)

    fun showErrorMessage(message: String?)

    fun showErrorMessage()

    fun showErrorMessage(stringId: Int)

    fun showEmptyMessage(message: String?)

    fun showEmptyMessage(stringId: Int)

    fun showEmptyMessage()

    fun showSnack()

    fun showSnack(stringId: Int)

    fun showSnack(message: String?)
}