package ru.rsb.mvp

import android.os.Bundle
import androidx.fragment.app.Fragment


/**
 * @author Maxim Berezin
 */
abstract class StateStorage<T : BaseState> : Fragment() {

    // data object we want to retain
    var data: T? = null

    // this method is only called once for this fragment
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // retain this fragment
        retainInstance = true
    }
}