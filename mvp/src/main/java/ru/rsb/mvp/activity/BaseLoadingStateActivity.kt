package ru.rsb.mvp.activity

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.google.android.material.snackbar.Snackbar
import ru.rsb.core.R
import ru.rsb.mvp.LoadingStateMvpView
import ru.rsb.mvp.widget.LoadingView

/**
 * @author Maxim Berezin
 */
abstract class BaseLoadingStateActivity : BaseActivity(), LoadingStateMvpView {

    var loadingView: LoadingView? = null

    override fun onDestroy() {
        loadingView = null
        super.onDestroy()
    }

    override fun setContentView(layoutResID: Int) {
        loadingView = LoadingView(this).apply {
            layoutParams =
                    FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        }

        val content = LayoutInflater.from(this).inflate(layoutResID, null)
        loadingView?.addView(content)

        super.setContentView(loadingView)
    }

    override fun setLoadingViewVisible(visible: Boolean) {
        if (visible) loadingView?.showLoading() else loadingView?.clearScreen()
    }

    override fun showErrorMessage(message: String?) {
        message?.run { loadingView?.showMessage(message) } ?: showErrorMessage()
    }

    override fun showErrorMessage(stringId: Int) {
        loadingView?.showMessage(stringId)
    }

    override fun showErrorMessage() {
        loadingView?.showMessage(R.string.base_loading_state_activity_error_occur)
    }

    override fun showEmptyMessage(message: String?) {
        message?.run { loadingView?.showMessage(this) }
            ?: showEmptyMessage()
    }

    override fun showEmptyMessage() {
        loadingView?.showMessage(R.string.base_loading_state_activity_no_data)
    }

    override fun showEmptyMessage(stringId: Int) {
        loadingView?.showMessage(stringId)
    }

    override fun showSnack() {
        window.decorView.findViewById<View>(android.R.id.content)
            .showSnackBar(getString(R.string.base_loading_state_activity_error_occur), resources.getInteger(R.integer.snack_delay))
    }

    override fun showSnack(stringId: Int) {
        window.decorView.findViewById<View>(android.R.id.content).showSnackBar(getString(stringId), resources.getInteger(R.integer.snack_delay))
    }

    override fun showSnack(message: String?) {
        message?.run {
            window.decorView.findViewById<View>(android.R.id.content).showSnackBar(this, resources.getInteger(R.integer.snack_delay))
        } ?: showSnack()
    }
}

/**
 *Показываем SnackBar
 */
fun View.showSnackBar(message: String, duration: Int) {
    Snackbar.make(this, message, duration).show()
}