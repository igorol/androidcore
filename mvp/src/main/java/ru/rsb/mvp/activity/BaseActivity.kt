package ru.rsb.mvp.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity


open class BaseActivity : AppCompatActivity() {
    private val debugMessage = { message: String -> Log.v(javaClass.simpleName, message) }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        debugMessage("onNewIntent")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        debugMessage("onCreate")
    }

    override fun onResume() {
        super.onResume()
        debugMessage("onResume")
    }

    override fun onPause() {
        super.onPause()
        debugMessage("onPause")
    }

    override fun onStart() {
        super.onStart()
        debugMessage("onStart")
    }

    override fun onStop() {
        super.onStop()
        debugMessage("onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        debugMessage("onDestroy")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        debugMessage("onActivityResult")
    }
}