package ru.rsb.mvp

import io.reactivex.disposables.CompositeDisposable

/**
 * @author Maxim Berezin
 */
abstract class BaseRxPresenter<V : MvpView> : BasePresenter<V>(), RxPresenter {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun getCompositeDisposable(): CompositeDisposable {
        return compositeDisposable
    }

    override fun dropView() {
        super.dropView()
        compositeDisposable.clear()
    }

}