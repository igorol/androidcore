package ru.rsb.mvp

/**
 * @author Maxim Berezin
 */
abstract class BaseStatefulPresenterImpl<V : MvpView, S : BaseState> : BaseStatefulPresenter<V, S> {

    protected open var view: V? = null
    protected open var state: S? = null

    var isInitialized: Boolean = false


    override fun takeView(view: V, state: S?) {
        this.view = view
        this.state = state
        initialize()
    }

    override fun dropView() {
        view = null
        isInitialized = false
    }


    private fun initialize() {
        if (!isInitialized) {
            isInitialized = true
            onInitialize()
        }
    }


    protected open fun onInitialize() {

    }
}