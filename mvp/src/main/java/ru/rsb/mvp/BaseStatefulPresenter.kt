package ru.rsb.mvp

/**
 * @author Maxim Berezin
 */
interface BaseStatefulPresenter<V : MvpView, S : BaseState> : Presenter {

    /**
     * Привязывает view к презентеру
     *
     * @param view Представление
     */
    fun takeView(view: V, state: S? = null)

    /**
     * Отвязывает view от презентера
     *
     */
    fun dropView()

    /**
     * Отдает инстанс state
     */
    fun createState(): S
}