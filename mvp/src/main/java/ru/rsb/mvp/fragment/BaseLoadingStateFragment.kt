package ru.rsb.mvp.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import ru.rsb.core.R
import ru.rsb.mvp.LoadingStateMvpView
import ru.rsb.mvp.activity.showSnackBar
import ru.rsb.mvp.widget.LoadingView

/**
 * Базовый фрагмент с прогресс баром и окном об ошибке.
 *
 * @author Maxim Berezin
 */
abstract class BaseLoadingStateFragment : Fragment(), LoadingStateMvpView {

    lateinit var loadingView: LoadingView

    /**
     * Вызывается до завершения [onCreateView].
     * Из-за особенностей фреймворка инициализация разметки следует проводить в этом методе
     *
     * Параметры однозначно передаются из метода [onCreateView] кроме
     * @param container - [LoadingView] растянутый на весь экран
     *
     * @return view с разметкой
     * */
    abstract fun onPreCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loadingView = LoadingView(context!!).apply {
            layoutParams =
                    FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        }
        val contentView = onPreCreateView(inflater, loadingView, savedInstanceState)
        if (contentView == null) throw IllegalArgumentException("contentView не должен быть null.")
        else return loadingView.apply { addView(contentView) }
    }

    //region LoadingStateMvpView
    override fun setLoadingViewVisible(visible: Boolean) {
        if (visible) loadingView.showLoading() else loadingView.clearScreen()
    }

    override fun showErrorMessage(message: String?) {
        message?.run { loadingView.showMessage(message) } ?: showErrorMessage()
    }

    override fun showErrorMessage(stringId: Int) {
        loadingView.showMessage(stringId)
    }

    override fun showErrorMessage() {
        loadingView.showMessage(R.string.base_loading_state_activity_error_occur)
    }

    override fun showEmptyMessage(message: String?) {
        message?.run { loadingView.showMessage(this) }
            ?: showEmptyMessage()
    }

    override fun showEmptyMessage(stringId: Int) {
        loadingView.showMessage(stringId)
    }

    override fun showEmptyMessage() {
        loadingView.showMessage(R.string.base_loading_state_activity_no_data)
    }

    override fun showSnack() {
        view?.showSnackBar(
            context!!.getString(R.string.base_loading_state_activity_error_occur),
            resources.getInteger(R.integer.snack_delay)
        )
    }

    override fun showSnack(stringId: Int) {
        view?.showSnackBar(context!!.getString(stringId), resources.getInteger(R.integer.snack_delay))
    }

    override fun showSnack(message: String?) {
        message?.run {
            view?.showSnackBar(this, resources.getInteger(R.integer.snack_delay))
        } ?: showSnack()

    }
    //endregion
}
