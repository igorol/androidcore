package ru.rsb.mvp

/**
 * @param <V> [MvpView], c которой работает презентер.
 * @author Maxim Berezin
 */
interface MvpPresenter<V : MvpView> : Presenter {

    /**
     * Привязывает view к презентеру
     *
     * @param view Представление
     */
    fun takeView(view: V)

    /**
     * Отвязывает view от презентера
     *
     */
    fun dropView()
}