package ru.rsb.mvp

/**
 * Скелетная реализация [MvpPresenter].
 *
 * @param <V> view, c которой работает презентер.
 * @author Maxim Berezin
 */
abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    protected open var view: V? = null
    var isInitialized: Boolean = false


    override fun takeView(view: V) {
        this.view = view
        initialize()
    }

    override fun dropView() {
        this.view = null
    }


    private fun initialize() {
        if (!isInitialized) {
            isInitialized = true
            onInitialize()
        }
    }


    protected open fun onInitialize() {

    }
}
