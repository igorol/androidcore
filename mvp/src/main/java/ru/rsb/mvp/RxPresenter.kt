package ru.rsb.mvp

import io.reactivex.disposables.CompositeDisposable

/**
 * @author Maxim Berezin
 */
interface RxPresenter {

    fun getCompositeDisposable(): CompositeDisposable

}