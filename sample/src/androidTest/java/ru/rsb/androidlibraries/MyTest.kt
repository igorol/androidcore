package ru.rsb.androidlibraries

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import org.junit.Test
import org.mockito.Mockito

/**
 * @author Vasilyev Evgeny.
 */
class MyTest {
    @Test
    fun testMediator() {
        val mediatorResource = MediatorLiveData<String>()
        val data = MutableLiveData<Int>()
        mediatorResource.addSource(data, Observer {
            println("mediator onChange: $it")
            assert(true)
        })

        data.value = 4
        mediatorResource.value = "hello world"
        val lifecycle = LifecycleRegistry(Mockito.mock(LifecycleOwner::class.java))
        mediatorResource.observe({ lifecycle }, {
            println("mediator resource observe: $it")
            assert(true)
        })
        assert(true)
    }
}