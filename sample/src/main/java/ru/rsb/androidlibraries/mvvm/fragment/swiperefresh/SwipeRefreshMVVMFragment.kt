package ru.rsb.androidlibraries.mvvm.fragment.swiperefresh

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.content_screen.view.*
import ru.rsb.androidlibraries.R
import ru.rsb.mvvm.BaseSwipeToRefreshMVVMFragment

/**
 * @author Vasilyev Evgeny.
 */
class SwipeRefreshMVVMFragment : BaseSwipeToRefreshMVVMFragment<SwipeRefreshViewModel>() {
    companion object {
        fun newInstance(): SwipeRefreshMVVMFragment =
            SwipeRefreshMVVMFragment()
    }

    val adapter: RVAdapter by lazy {
        RVAdapter(
            listOf(
                "Привет,",
                "это",
                "самый",
                "настоящий",
                "список!"
            ), context!!
        )
    }

    override fun onPreCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return (inflater.inflate(R.layout.recycler_view, container, false) as RecyclerView).apply {
            this@apply.adapter = this@SwipeRefreshMVVMFragment.adapter
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
        }
    }

    override fun onCreateContentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.content_screen, container, false).apply {
            this@apply.errorInvokeButton.setOnClickListener { viewModel.invokeErrorOnView() }
        }
    }

    override fun onCreateViewModel(): SwipeRefreshViewModel {
        viewModel = ViewModelProviders.of(this).get(SwipeRefreshViewModel::class.java)
        return viewModel
    }

    override fun onObserve(viewModel: SwipeRefreshViewModel) {
        viewModel.data.observe(this@SwipeRefreshMVVMFragment, Observer { data ->
            if (data != null){
                adapter.setData(data)
            }
        })
    }
}