package ru.rsb.androidlibraries.mvvm

import android.annotation.SuppressLint
import androidx.lifecycle.MediatorLiveData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.rsb.androidlibraries.R
import ru.rsb.mvvm.helpers.Event
import ru.rsb.mvvm.viewmodels.BaseViewModel
import ru.rsb.mvvm.viewmodels.BaseViewModel.ViewState.STATES.CONTENT
import ru.rsb.mvvm.viewmodels.BaseViewModel.ViewState.STATES.ERROR
import java.util.concurrent.TimeUnit

/**
 * @author Vasilyev Evgeny.
 */
class MVVMViewModel : BaseViewModel() {

    @SuppressLint("CheckResult")
    fun onLoadingButtonClick() {
        showLoading.value = true
        Single
            .timer(5, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({ showLoading.value = false }, {})
    }

    fun onSnackButtonClick() {
        showSnack.value = Event("а вот и snackbar!")
    }

    @SuppressLint("CheckResult")
    fun onMessageButtonClick() {
        state.value = ViewState(ERROR, ErrorMessageEvent(R.string.sample_error, R.drawable.ic_android_black_24dp))
        Single
            .timer(5, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                state.value =
                    ViewState(CONTENT, null)
            }, {})
    }

    override val state: MediatorLiveData<ViewState> = MediatorLiveData()


}