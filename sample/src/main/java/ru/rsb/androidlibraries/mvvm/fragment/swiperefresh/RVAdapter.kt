package ru.rsb.androidlibraries.mvvm.fragment.swiperefresh

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recyclerview_item.view.*
import ru.rsb.androidlibraries.R

/**
 * @author Vasilyev Evgeny.
 */
class RVAdapter(private var list: List<String>, context: Context) : RecyclerView.Adapter<RVAdapter.ViewHolder>() {

    private var layoutInflater: LayoutInflater? =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        with(layoutInflater?.inflate(R.layout.recyclerview_item, parent, false)){
            ViewHolder(this!!)
        }

    override fun getItemCount(): Int = list.count()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.text.text = list[position]
    }

    fun setData(newData: List<String>){
        list = newData
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val text = itemView.textView
    }
}