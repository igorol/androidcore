package ru.rsb.androidlibraries.mvvm.fragment

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.rsb.androidlibraries.R

/**
 * @author Vasilyev Evgeny.
 */
class ActivityWithMVVMFragment : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_with_fragment)


        if (supportFragmentManager.findFragmentById(R.id.rootContainer) == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.rootContainer, MVVMFragment())
                .commit()
        }
    }
}