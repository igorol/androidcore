package ru.rsb.androidlibraries.mvvm.fragment.swiperefresh

import android.annotation.SuppressLint
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import ru.rsb.androidlibraries.R
import ru.rsb.mvvm.viewmodels.BaseRefreshViewModel
import java.util.concurrent.TimeUnit

/**
 * @author Vasilyev Evgeny.
 */
class SwipeRefreshViewModel : BaseRefreshViewModel() {
    val data = MutableLiveData<List<String>>()

    override val state = MediatorLiveData<ViewState>()

    init {
        state.addSource(data, Observer {
            if (it.isNullOrEmpty()) state.value = ViewState(ViewState.ERROR, ErrorMessageEvent(R.string.sample_error))
            else state.value = ViewState(ViewState.CONTENT, null)

            showLoading.value = false
        })
    }

    @SuppressLint("CheckResult")
    override fun onRefresh() {
        Single.just(
            listOf(
                "Это",
                "совершенно",
                "новый",
                "список.",
                "Никто",
                "не",
                "заметит",
                "подмены"
            )
        )
            .delay(1400, TimeUnit.MILLISECONDS)
            .observeOn(Schedulers.io())
            .subscribe({
                data.postValue(it.shuffled())
            }, {})
    }

    fun invokeErrorOnView() {
        data.value = null
    }
}