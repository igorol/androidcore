package ru.rsb.androidlibraries.mvvm.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_mvvm.view.*
import ru.rsb.androidlibraries.R
import ru.rsb.androidlibraries.mvvm.MVVMViewModel
import ru.rsb.mvvm.BaseLoadingStateMVVMFragment

/**
 * @author Vasilyev Evgeny.
 */
class MVVMFragment : BaseLoadingStateMVVMFragment<MVVMViewModel>() {
    override fun onPreCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.activity_mvvm, container, false).apply {
            this.loadingButton.setOnClickListener { viewModel.onLoadingButtonClick() }
            this.snackBar.setOnClickListener { viewModel.onSnackButtonClick() }
            this.message.setOnClickListener { viewModel.onMessageButtonClick() }
        }

    override fun onCreateViewModel(): MVVMViewModel = ViewModelProviders.of(this).get(MVVMViewModel::class.java)


}