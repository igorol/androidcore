package ru.rsb.androidlibraries

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_navigation.*
import ru.rsb.androidlibraries.mvp.baseloading.ActivityWithBaseFragment
import ru.rsb.androidlibraries.mvp.baseloading.MainActivity
import ru.rsb.androidlibraries.mvvm.fragment.ActivityWithMVVMFragment
import ru.rsb.androidlibraries.mvvm.fragment.swiperefresh.ActivityWithSwipeToRefreshFragment

class NavigationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        baseFragment.setOnClickListener { startActivity(Intent(this, ActivityWithBaseFragment::class.java)) }
        baseActivity.setOnClickListener { startActivity(Intent(this, MainActivity::class.java)) }
        mvvmFragment.setOnClickListener { startActivity(Intent(this, ActivityWithMVVMFragment::class.java)) }
        swipeToRefreshFragment.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    ActivityWithSwipeToRefreshFragment::class.java
                )
            )
        }
    }
}
