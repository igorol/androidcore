package ru.rsb.androidlibraries.mvp.baseloading

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.rsb.androidlibraries.R

class ActivityWithBaseFragment : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_with_fragment)

        if (supportFragmentManager.findFragmentById(R.id.rootContainer) == null) {
            supportFragmentManager.beginTransaction()
                .add(R.id.rootContainer, ExampleFragment())
                .commit()
        }
    }
}
