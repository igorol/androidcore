package ru.rsb.androidlibraries.mvp.baseloading


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_example.*
import ru.rsb.androidlibraries.R
import ru.rsb.mvp.fragment.BaseLoadingStateFragment
import java.util.concurrent.TimeUnit

class ExampleFragment : BaseLoadingStateFragment() {

    override fun onPreCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_example, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        loadingButton.setOnClickListener { setLoading() }
        loadingButtonString.setOnClickListener { setLoadingString() }
        loadingButtonInt.setOnClickListener { setLoadingInt() }

        val imageDrawable = context?.getDrawable(R.drawable.ic_android_black_24dp)

        errorButtonString.setOnClickListener {
            loadingView?.showMessage("Строка с сообщением")
            clearScreen()
        }
        errorButtonInt.setOnClickListener {
            loadingView?.showMessage("Константа с сообщением")
            clearScreen()
        }
        errorButtonIntInt.setOnClickListener {
            loadingView?.showMessage(
                R.string.sample_message_with_image,
                R.drawable.ic_android_black_24dp
            )
            clearScreen()
        }
        errorButtonIntDraw.setOnClickListener {
            loadingView?.showMessage(
                R.string.sample_message_with_drawable,
                imageDrawable
            )
            clearScreen()
        }
        errorButtonStringInt.setOnClickListener {
            loadingView?.showMessage(
                "Строка с сообщением и картинка из res/drawable",
                R.drawable.ic_android_black_24dp
            )
            clearScreen()
        }
        errorButtonStringDraw.setOnClickListener {
            loadingView?.showMessage(
                "Строка с сообщением и картинка Drawable",
                imageDrawable
            )
            clearScreen()
        }
    }

    fun setLoading() {
        loadingView?.showLoading()
        clearScreen()
    }

    fun setLoadingString() {
        loadingView?.showLoading("Строка во время загрузки")
        clearScreen()
    }

    fun setLoadingInt() {
        loadingView?.showLoading(R.string.sample_loading_message)
        clearScreen()
    }

    @SuppressLint("CheckResult")
    fun clearScreen() {
        Observable.timer(2, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe { loadingView?.clearScreen() }
    }
}
